from hashlib import sha1
from binascii import hexlify,unhexlify
import sys
import M2Crypto
from M2Crypto import RSA
import struct
import math
import cStringIO
import time
import re
import os
import cProfile

class QuickSTFS:
    def __init__(self, f):
        global startTime
        #print 'entering init: '+str(time.time()-startTime)
        if type(f) is str:
            self.fileName = f
            f = open2(f, 'rb+')
        else:
            self.fileName = f.name
            f = streamWrap(f)
        self.file = f
        #print 'loading RSA: '+str(time.time()-startTime)
        #nah, embed the key instead
        #self.signer = RSA.load_key('RSAParameters/rsaparams.pem')
        self.loadSTFS()

    def extractFirst(self,destination=None,ascsio=False):
        return self.extractFile(self.files[0],destination=destination,ascsio=ascsio)

    def loadSTFS(self):
        global startTime
        f=self.file
        self.setSignerAndCertificate()
        #no more than 28900 level0 hash tables or 170 level 1 hash tables can fit. Though, it's likely to almost never be this big for the files handled by the program.
        #The alternative would be to check the size of the bytearray when updating the bitmap. Would the checking be better than creating a bytearray this big that might not need to be?
        self.usedHashTables = [bytearray(28900),bytearray(170)]
        #not used, but might be to keep track of the farthest data block
        self.maxBlock = 0
        #same as above for hashtables
        self.maxHashTable = [0,0]
        # no more than 614125 data blocks can fit
        self.bitmap = bytearray(614125)
        f.seek(0x0)
        self.headerInfo = self.headerInfoClass(f.read(0xA000))
        #finds disposition of master hash table (furthest up, level1 usually)
        self.findHashDisposition()
        self.readFileTable()
        self.findUsedBlocks()
        self.checkSignature()

    def close(self):
        try:
            self.file.close()
        except Exception as ex:
            pass

    def open(self):
        try:
            if self.file.closed:
                self.file=open2(self.fileName,'rb+')
        except Exception as ex:
            raise Exception("Couldn't reopen file")

    def setSignerAndCertificate(self):
        """
        Easier to embed these than to read them - going into memory anyway. Don't worry, these don't need to be private 
        """
        self.signer = M2Crypto.RSA.load_key_string("""-----BEGIN PRIVATE KEY-----
MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKMdbOX6lf3okCH6
0QxkGSuGWJsXKxAFuNH4TO9TTNVOXK6G75J7kNHgYv18VFWe4Oe++j+eFW9sOE6v
Bwxhq1FeI1MUGIjLb8vF1jD0Bu0kI+8lbQCRdySb5aPAJ5DCl/d0nW8Xg361N95R
6Ncc4VbZVsjDwyCdZMMvjJGSMG/bAgMBAAECgYBR7B+dVibC/BCmZ2TLOm1NoedO
qELw9P36Zu/HjhAv5ByjHdDOOS7DGS3QWHR5rAjnkMGsLcbrR+g9z0xt/1Fl1G69
DxV5N5XEr5CeK1CKCiJKs0HliYBzzfohAvXdMN0HKm80B4GXfrL7cunqwYg5rEgr
qE381+2b+d7CRZNMTAJBAMznXf5ytv3nHeMaDqwzerkh6IqEm9qfHlg0aHqxHX4c
GFJle5eOp2qd7lp3UjtxjzPQSV7DMDlyNr8d2fIk6HECQQDLylh01ANikwZQH0L2
qlk2p6Hzl1yayGonz4UFKmZBan8vhMgYE8YdjccyL3IZP6TtcedhwM9hrougaKd9
gyMLAkBMynTmdDVySFhiERTook5e7X9J0lLahwGHSvTQ7mnAJmVTE+dSsEq74T4/
tzIhRvjFEU0972a2UMCFtXlFj2FxAkEAr9xG51KKNUehHAVOOSSZ5kNUy6vj2yJ2
ETLQnLuREISBixUvwy9VOO2/ZzxwXv+AKPOxc7b6f1Yr4dpOJ07CLwJAKGq70ZOV
lBpu7dcOwGErwu/hhj00EohvlKRIbsmHHkYARgBSjp9HwIyrvEmsWxPy7CeNG25R
BqbxYhrreC6ISA==
-----END PRIVATE KEY-----""")
        self.certificate = '\x01\xa8\t\x12\xba&\xe3X803395-001\x00\x00\x00\x00\x00\x00\x00\x00\x00\x0209-18-06\x00\x01\x00\x01\xc3/\x8c\x91\x920o\xdb\xd9V\xc8\xc3\xc3 \x9dd7\xdeQ\xe8\xd7\x1c\xe1V\xf7t\x9do\x17\x83~\xb5\x9b\xe5\xa3\xc0\'\x90\xc2\x97#\xef%m\x00\x91w$\xcb\xc5\xd60\xf4\x06\xed$^#S\x14\x18\x88\xcbo8N\xaf\x07\x0ca\xabQ\xe7\xbe\xfa?\x9e\x15ol\xe0b\xfd|TU\x9e\xe0\\\xae\x86\xef\x92{\x90\xd1\xd1\xf8L\xefSL\xd5N\x86X\x9b\x17+\x10\x05\xb8\x90!\xfa\xd1\x0cd\x19+\xa3\x1dl\xe5\xfa\x95\xfd\xe8\xf9p\r*\x899\x9e\xd5Ne\x87D\xf9O \x90\x89A7P.\xf80\x08\xccn\xcd\xd1W\xe7\xc3\xb7\x96\xb0*\x80Y\xcb~C\xfb\xdb~\x0c\xefl^\x00\x0b\x1e\x87\xe1\x02d\xa7\x08$2\xb9S\x15\x00\xe9\xe3S\x0c\x15\xe1]Y\xc6\t\xab\xd1s\xb5\xee\xc5\xe7P\xbc\xc2\xb2%\x98\xba\xa0\n\x84\xf4\xf8-\x1a\xd2\xc9\x7f\xdc\xcf]\x02!\x9a%\xe0i\x11l\xfc\x88\x06\x01I\xf4t@\x8d\xd8\x91\xdb\x83\xc9`\xce\r\x7f\x97\xaa*6\xa5\xf0\x0c\x10c\xe9\xa99O\xbbGlD"\xf1\xbe:I\x01\xed[G\x00C!\xbd\xfb\xb2\x95\x9a_\xb4F\xf4\xa7\x12$K\x0b\x7f\xb8\x8e\xbbR\x83"X\x1e\x06\xb7\xadz:\x16~\xc8\xd77\x81\x9e\x8a\xf2\xc4f\x08\x88\xfe\xa7\x0e\x8f\x9d\x87_\x0e{H\x9a\x06b\xf7$%\xcd\xb0Osh\x97\x0c\xe4\xad\xe8U\x9a\xb4\xfae\xb5\xa3X\xfe\x81@T\xaa\x1f\x00*\xf1\xdd\x8a\x1fEN\x9d\xff\x82FZZ\x90%\xa0X\x0f\xf2\''
        

    def rehashAndResign(self):
        f=self.file
        for i in self.toHash():
            hsh = sha1(f.seekRead(i[0],i[1])).digest()
            f.seek(i[2])
            f.write(hsh)
        self.fixSignature()

    def checkHashes(self,falseOnly=False,exclude=None):
        """
        for debugging
        """
        f=self.file
        for i in self.toHash():
            if exclude == None or i[3] not in exclude:
                verified = sha1(f.seekRead(i[0],i[1])).digest() == f.seekRead(i[2],0x14)
                if not falseOnly or not verified:
                    print 'To hash: \n\toffset: {0}\n\tlength: {1}\n\tVerificationOffset: {2}\n\tType: {3}\n\tverified: {4}'.format(hex(i[0]),hex(i[1]),hex(i[2]),i[3],verified)

    def toHash(self):
        """
            return a tuple of offset of data to be hashed, length of data, place to write hash, type of data
        """
        pass
        for i in self.getUsedBlocks():
            #get the level0 locations to verify and locations to write to
            yield (self.getOffsetToVerify(i),0x1000,self.getHashBlockOffset(i),'data')
        for lvl in range(0,2):
            for b in self.getUsedHashTables(lvl):
                yield (self.getOffsetToVerify(b,lvl+1),0x1000,self.getHashBlockOffset(b,lvl+1),'level'+str(lvl))
        yield (self.masterHashOffset,0x1000,0x381,'masterHash')
        yield (0x344,0x9CBC,0x32C,'contentId')

    def reallocate(self):
        """
        Use up all unallocated blocks and truncate file
        """
        freegen = self.getFreeBlocks()
        freeblock = freegen.next()
        for f in self.files:
            flgen = self.getStreamPath(f.startBlock,f.blockLength)
            if f.startBlock > freeblock:
                #TODO: move this into self.moveBlockFull 
                self.setNextDataBlock(freeblock, self.getNextDataBlock(f.startBlock))
                self.moveBlock(f.startBlock,freeblock)
                self.setUnusedBlocks(f.startBlock)
                self.setUsedBlocks(freeblock)
                f.startBlock = freeblock
                freeblock = freegen.next()
            prevblock = f.startBlock
            for b in self.getStreamPath(f.startBlock,f.blockLength):
                if b > freeblock:
                    #self.setNextDataBlock(freeblock,self.getNextDataBlock(b))
                    #self.moveBlock(b,freeblock)
                    #self.setUnusedBlocks(b)
                    #self.setUsedBlocks(freeblock)
                    #self.setNextDataBlock(prevblock,freeblock)
                    self.moveBlockFull(prevblock,b,freeblock)
                    freeblock = freegen.next()
                prevblock = b
        #let's truncate the file so it's no bigger than it needs to be. Hash tables always come before the data they hash, so if we know a block is the last used, we can safely truncate past it. if b > freeblock, then the above code switched b to the freeblock. Otherwise, freeblock is unused and the last data block
        if b > freeblock:
            last = freegen.next()
            self.file.truncate(self.getDataBlockOffset(last))
        else:
            self.file.truncate(self.getDataBlockOffset(freeblock))
        self.headerInfo.descriptor.totalUnallocatedBlockCount = 0
        #update the unallocated block count to 0
        self.file.seek(0x399)
        self.file.writeInt(0)
        self.writeFileTable()
        self.rehashAndResign()

    def moveBlockFull(self,prevblock,oldblock,newblock):
        """
        Move a block, adjust the "next block" int in the hash table pointing to the block to be moved, right the data to the new block, set the next block to the one in the block-to-be-moved's hashtable
        """
        self.setNextDataBlock(newblock,self.getNextDataBlock(oldblock))
        self.moveBlock(oldblock,newblock)
        self.setUnusedBlocks(oldblock)
        self.setUsedBlocks(newblock)
        self.setNextDataBlock(prevblock,newblock)

    def moveBlock(self,oldblock,newblock):
        #Write's a block into another block. Should move this in with moveblockfull
        self.file.seekWrite(self.getDataBlockOffset(newblock),self.file.seekRead(self.getDataBlockOffset(oldblock),0x1000))

    #start bitmap methods
    def findUsedBlocks001(self):
        """
        switched to bytearrays from long bitmaps, still testing new methods
        """
        mlevel = self.masterHashLevel
        hashTables = [0,0,0]
        for xfile in self.files:
            for b in self.getFilePath(xfile): 
                if b+1 > self.maxBlock:
                    self.maxBlock = b+1
                self.bitmap |= 1<<b
                if mlevel>0:
                    hashTables[0] = self.getHashTableStart(b)
                    if hashTables[0]+1>self.maxHashTable[0]:
                        self.maxHashTable[0] = hashTables[0]+1
                    self.usedHashTables[0] |= 1 <<hashTables[0]
                    if mlevel>1:
                        hashTables[1] = self.getHashTableStart(hashTables[0])
                        if hashTables[1]+1 > self.maxHashTable[1]:
                            self.maxHashTable[1] = hashTables[1] + 1
                        self.usedHashTables[1] |= 1<<hashTables[1]
        self.bitmap |= 1<<self.headerInfo.descriptor.fileTableBlockNumber
        if self.headerInfo.descriptor.fileTableBlockNumber+1 > self.maxBlock:
            self.maxBlock = self.headerInfo.descriptor.fileTableBlockNumber+1
        #print 'done' 

    def findUsedBlocks(self):
        """
        Create bytearray bitmap of all the used data blocks and used hash tables (for resigning and adding files, etc.)
        """
        mlevel = self.masterHashLevel
        hashTables = [0,0,0]
        for xfile in self.files:
            for b in self.getFilePath(xfile): 
                if b+1 > self.maxBlock:
                    self.maxBlock = b+1
                self.setUsedBlocks(b)
                if mlevel>0:
                    hashTables[0] = self.getHashTableStart(b)
                    if hashTables[0]+1>self.maxHashTable[0]:
                        self.maxHashTable[0] = hashTables[0]+1
                    self.setUsedHashTables(0,hashTables[0])
                    if mlevel>1:
                        hashTables[1] = self.getHashTableStart(hashTables[0])
                        if hashTables[1]+1 > self.maxHashTable[1]:
                            self.maxHashTable[1] = hashTables[1] + 1
                        self.setUsedHashTables(1,hashTables[1])
        self.setUsedBlocks(self.headerInfo.descriptor.fileTableBlockNumber)
        if self.headerInfo.descriptor.fileTableBlockNumber+1 > self.maxBlock:
            self.maxBlock = self.headerInfo.descriptor.fileTableBlockNumber+1
        #print 'done' 

    def isBlockUsed(self,block):
        return self.bitmap[block//8] >> (block%8) & 1 == 1

    def isBlockUsed001(self,block):
        """
        switched to bytearrays from long bitmaps, still testing new methods
        """
        return (self.bitmap >> block )&1 ==1

    def getFreeBlocks(self):
        """
        Gets all the free, allocated blocks but eventually returns yet unallocated blocks
        """
        bmap = self.bitmap
        for b in range(0,len(bmap)<<3):
            if bmap[b//8] >> (b%8) & 1 == 0:
                yield b

    def getFreeBlocksAndOverwrite(self, o):
        """
        Yield the blocks of a stream to be overwritten (o) and then return free block. Also return if we used all the blocks from the stream
        """
        if not o == None:
            self.files.remove(o)
            for b in self.getStreamPath(o.startBlock,o.blockLength):
                yield b, False
        for b in self.getFreeBlocks():
            yield b, True

    def getFreeBlocks001(self):
        """
        switched to bytearrays from long bitmaps, still testing new methods
        """
        bmap = self.bitmap
        i=0
        while True:
            if (bmap>>i)&1 == 0:
                yield i
            i+=1

    def setUsedHashTables(self,level,*args):
        for b in args:
            self.usedHashTables[level][b//8] |= 1 << (b%8)

    def getUsedHashTables(self,level):
        bmap = self.usedHashTables[level]
        for b in range(0,self.maxHashTable[level]):
            if bmap[b//8] >> (b%8) & 1 == 1:
                yield b

    def setUsedHashTables001(self,level=0,*args):
        for b in args:
            self.usedHashTables[level] |= 1<<b

    def getUsedHashTables001(self,level):
        bmap = self.usedHashTables[level]
        i=0
        while i < self.maxHashTable[level]:
            if (bmap >> i) &1 ==1:
                yield i
            i+=1

    def setUnusedBlocks(self,*args):
        for b in args:
            self.bitmap[b//8] &= ~(1<<(b%8))

    def setUsedBlocks(self,*args):
        for b in args:
            self.bitmap[b//8] |= 1<<(b%8)

    def getUsedBlocks(self):
        bmap = self.bitmap
        i=0
        for i in range(0,self.maxBlock):
            if bmap[i//8] >> (i%8) & 1 == 1:
                yield i

    def setUnusedBlocks001(self,*args):
        for b in args:
            self.bitmap = ~(1<<b)

    def setUsedBlocks001(self,*args):
        for b in args:
            self.bitmap |= 1<<b

    def getUsedBlocks001(self):
        bmap = self.bitmap
        i=0
        while i < self.maxBlock:
            if (bmap >> i) &1 ==1:
                yield i
            i+=1

    #end bitmap functions

    def findHashDisposition(self):
        """
        Find level and offset of the master hash table. 

        Each hash table can hold up to 0xAA hashes, nor more than the 0xA9th can fit (counting from zero). Once a table is filled, it's hashed by the next level up. The master hash is whichever the highest level hash is, and is itself hashed in the header. The level2 can't be filled without exceeding max file size
        """
        f=self.file
        #read block separation byte
        dsp = self.headerInfo.descriptor.masterDisposition
        self.masterHashOffset=dsp
        sz = self.headerInfo.descriptor.totalAllocatedBlockCount

        if sz < 0xAA:
            self.masterHashOffset += 0xA000 
            self.masterHashLevel = 0
        elif sz < 0x70E4:
            self.masterHashOffset += 0xB6000
            self.masterHashLevel = 1
        elif sz < 0x4AF768:
            self.masterHashOffset += 0x7244000
            self.masterHashLevel = 2
        else:
            raise Exception("File too large")

        ### Each hash disposition seems to be unique, so never mind setting a single one

        #f.seek(self.masterHashOffset)
        #checking the seventh bit at offset 0x14 of the master hash
        #self.hashDisposition = ((struct.unpack('b',f.read(0x18)[0x14])[0]>>6) & 1)*0x1000

    def extractFile(self,o=None,destination=None,ascsio=False,ofs=None,idx=None):
        try:
            if not ofs == None:
                xfile = next(x for x in self.files if x.entryOffset == ofs)
            elif not idx == None:
                xfile = self.files[idx]
            elif not o == None:
                xfile = o
            else:
                raise Exception("Missing file identifier")
            if destination == None and ascsio == False:
                raise Exception("Missing place to save file to")
        except StopIteration:
            raise Exception("File does not exist with that offset")
        f=self.file
        if ascsio:
            w = cStringIO.StringIO()
        else:
            w = open(destination+xfile.realName,'wb')
        for b in self.getFilePath(xfile):
            f.seek(self.getDataBlockOffset(b))
            w.write(f.read(0x1000))
        w.truncate(xfile.size)
        if ascsio:
            return w
        else:
            w.close()
            return True

    def getFilePath(self, xfile):
        """
        Find hashblock of data, read next block, etc.
        """
        f=self.file
        block = xfile.startBlock
        sz = xfile.blockLength
        ofs = self.getDataBlockOffset(block)
        for i in xrange(0,sz):
            try:
                #0xFFFFFF = -1, end of file stream, no more blocks
                if block == -1:
                    break
                yield block
                f.seek(ofs)
                ofs = self.getHashBlockOffset(block)
                f.seek(ofs+0x15)
                block = f.readInt24()
                ofs = self.getDataBlockOffset(block)
            except Exception as ex:
                #catchall for reading something that wasn't the next block or any other possible error.
                raise Exception ("Incorrect file paths.")

    def getStreamPath(self, startBlock, blockCount):
        """
        Works on files and on the filetable.
        """
        f = self.file
        ofs = self.getDataBlockOffset(startBlock)
        block = startBlock
        for i in range(0, blockCount):
            try:
                #end of stream...
                if block == -1:
                    break
                yield block
                f.seek(ofs) 
                block = self.getNextDataBlock(block)
            except Exception as ex:
                raise Exception ("Incorrect file paths.")

    def getNextBlockOffset(self, block):
        """
        Better in most cases to get the next data block instead, left just in case
        """
        self.file.seek(self.getHashBlockOffset(block)+0x15)
        return self.getDataBlockOffset(self.file.readInt24())

    def getNextDataBlock(self, block):
        self.file.seek(self.getHashBlockOffset(block)+0x15)
        rtn = self.file.readInt24()
        self.file.seek(self.file.tell()-3)
        return rtn

    def setNextDataBlock(self,block,nextblock):
        self.file.seek(self.getHashBlockOffset(block)+0x15)
        self.writeInt24(nextblock)

    def readFileTable001(self):
        """
        Replaced with possibly better method?
        """
        f = self.file
        invalidChars = '[\x01-\x1F\x22\x2A\x2F\x3A\x3C\x3E\x3F\x5C\x7C\x7F-\xFF]'
        fileBlock = self.headerInfo.descriptor.fileTableBlockNumber
        self.files = []
        self.fileTableOffset = ofs = self.getDataBlockOffset(fileBlock)
        f.seek(ofs)
        fileBlockCount = self.headerInfo.descriptor.fileTableBlockCount
        while ofs < ofs+0x1000 * fileBlockCount:
            tmp = f.read(0x40)

            if tmp[0] == '\x00' or not re.search(invalidChars,tmp[:0x28]) is None:
                break
            self.files.append(self.FileEntry(ofs,tmp))
            ofs+=0x40

    def readFileTable(self):
        """
        TODO: add directory support.
        """
        f = self.file
        invalidChars = '[\x01-\x1F\x22\x2A\x2F\x3A\x3C\x3E\x3F\x5C\x7C\x7F-\xFF]'
        self.files = []
        for block in self.getStreamPath(self.headerInfo.descriptor.fileTableBlockNumber, self.headerInfo.descriptor.fileTableBlockCount):
            ofs = self.getDataBlockOffset(block)
            #print 'offset: {0}'.format(ofs)
            for i in range(0,0x40):
                f.seek(ofs)
                tmp = f.read(0x40)
                if tmp[0] == '\x00' or not re.search(invalidChars , tmp[:0x28]) is None:
                    break
                self.files.append(self.FileEntry(ofs,tmp))
                ofs+=0x40

    def deleteFile(self,idx=None,ofs=None,block=None,name=None):
        try:
            if not idx == None:
                xfile = self.files[idx]
            if not ofs == None:
                xfile = next(fl for fl in self.files if fl.entryOffset == ofs)
            elif not block == None:
                xfile = next(fl for fl in self.files if fl.startBlock == block)
            elif not name == None:
                xfile = next(fl for fl in self.files if fl.realName == name)
        except StopIteration:
            raise Exception("File does not exist with those parameters")
        self.files.remove(xfile)
        #TODO: should I add findUsedBlocks to set the now unused blocks as unused or would that exceed the expected functionality of the method?
        #^^same goes for rehashing and resigning / rewriting the file table

    def deleteFile001(self,ofs=None,block=None,name=None):
        """
        Would overwrite the entry in the file table. Better to just rewrite the file table when needed 
        """
        try:
            if not ofs == None:
                sz = next(fl for fl in self.files if fl.entryOffset == ofs).blockLength
            elif not block == None:
                sz = next(fl for fl in self.files if fl.startBlock == block).blockLength
        except StopIteration:
            raise Exception("File does not exist with that offset")
        f=self.file
        start = self.fileTableOffset
        tablelen = 0x1000*self.headerInfo.descriptor.fileTableBlockCount
        f.seek(start)
        tmp=f.read(ofs-start)
        f.seek(ofs+0x40)
        tmp+=f.read(tablelen - (ofs+0x40))
        f.seek(start)
        f.write(tmp + '\x00'*(tablelen-len(tmp)))
        self.headerInfo.descriptor.totalAllocatedBlockCount -= sz

        f.seek(0x395)
        f.write(struct.pack('>i',self.headerInfo.descriptor.totalAllocatedBlockCount))
        self.readFileTable()

    def replaceFile(self,path,idx=None,ofs=None,block=None,name=None):
        #replaced by add file with overwrite parameter? Might still use at some point?


        """
        Combination of deleting the old file and writing the new one.

        TODO: Either set the blocks of the deleted file as unused or overwrite its blocks


        """
        try:
            if not idx == None:
                xfile = self.files[idx]
            if not ofs == None:
                xfile = next(fl for fl in self.files if fl.entryOffset == ofs)
            elif not block == None:
                xfile = next(fl for fl in self.files if fl.startBlock == block)
            elif not name == None:
                xfile = next(fl for fl in self.files if fl.realName == name)
        except StopIteration:
            raise Exception("File does not exist with those parameters")
        self.deleteFile(idx)
        self.addFile(path)

    def addFile(self,pth,overWrite = None):
        try:
            fentry = self.FileEntry()
            fl = open2(pth,'rb')
            fentry.realName = os.path.basename(pth).replace(' ','_')
            fentry.name = fentry.realName.ljust(0x28,'\x00')
            sz = os.path.getsize(pth)
        except Exception as ex:
            #Catchall, should improve
            raise Exception("Problem with file")
        f=self.file
        #directory plus unknown flags, seventh and eigth bit (counting from 1)
        fentry.flags=0
        fentry.blockLength =  int(math.ceil(float(sz)/0x1000))
        fentry.blockLength2 = fentry.blockLength
        #We're going to be allocating its blocks
        if not overWrite == None:
            diff = fentry.blockLength - overWrite.blockLength
        else:
            diff = fentry.blockLength
        if diff <= self.headerInfo.descriptor.totalUnallocatedBlockCount:
            self.headerInfo.descriptor.totalUnallocatedBlockCount -= diff

        else:
            self.headerInfo.descriptor.totalAllocatedBlockCount += diff - self.headerInfo.descriptor.totalUnallocatedBlockCount
            self.headerInfo.descriptor.totalUnallocatedBlockCount = 0
        #inside root dir
        fentry.path = -1
        fentry.size = sz
        #incorrect time format according to py360, will fix
        fentry.updateTime = fentry.accessTime = int(time.time())
        #If overwrite is None, just gets free blocks
        gen = self.getFreeBlocksAndOverwrite(overWrite)
        #got all blocks from file to be overwritten?
        end = False
        block, end = gen.next()
        fentry.startBlock = block
        for i in range(0,fentry.blockLength):

            if block+1 > self.maxBlock:
                self.maxBlock = block+1
            #print "{0}: block: {1}\t{2}: {3}".format(i,block,hex(self.getDataBlockOffset(block)),hex(self.getHashBlockOffset(block)))
            f.seek(self.getDataBlockOffset(block))
            tmp = fl.read(0x1000)
            #Find a way to do this for only the last block? - All others are going to be 0x1000
            f.write(tmp.ljust(0x1000,'\x00'))
            self.setUsedBlocks(block)
            f.seek(self.getHashBlockOffset(block))
            f.write(sha1(tmp).digest())
            #bit flag for used block
            #print "writing 80 at {0}".format(hex(f.tell()))
            f.write('\x80')
            #print "after: {0}".format(hex(f.tell()))
            #last block, end of stream
            if i ==fentry.blockLength-1:
                block=-1
            else:
                block, end = gen.next()
            #print "writing: {0} at ofs: {1}".format(hexlify(f.makeInt24(block)),hex(f.tell()))
            f.writeInt24(block)
            #f.seek(f.tell()-3)
            #print "wrote: {0}".format(hexlify(f.read(3)))
        self.files.append(fentry)

        f.seek(0x395)
        f.writeInt(self.headerInfo.descriptor.totalAllocatedBlockCount)
        f.writeInt(self.headerInfo.descriptor.totalUnallocatedBlockCount)
        if end == False:
            for b, end in gen:
                if end == True:
                    break
                else:
                    self.setUnusedBlocks(b)
        self.writeFileTable()
        self.rehashAndResign()

    def writeFileTable(self):
        gen = self.getStreamPath(self.headerInfo.descriptor.fileTableBlockNumber,self.headerInfo.descriptor.fileTableBlockCount)
        f = self.file
        end = False
        cnt = 0
        b=gen.next()
        f.seek(self.getDataBlockOffset(b))
        for i in xrange(0,len(self.files)):
            #according to free60, "The listing ends with an entry consisting of only null bytes." 
            #each entry is 0x40 bytes, 0x40 of such entries can fit in 0x1000. Does that mean that If there are 0x40, that 0x3F go in the first block and I allocate another for the last?
            if cnt == 0x40:
                prev = b
                try:
                    b = gen.next()
                except StopIteration as ex:
                    gen = self.getFreeBlocks()
                    b = gen.next()
                    end = True
                f.seek(self.getDataBlockOffset(b))
                cnt = 0
                self.setNextDataBlock(prev,b)
            cnt += 1
            xfile = self.files[i]
            #print 'writing at offset {0} name: {1} \n\n'.format(f.tell(),hexlify(xfile.name))
            f.write(xfile.realName[:0x28].ljust(0x28,'\x00'))
            f.writeByte(len(xfile.realName)|xfile.flags,False)
            f.writeInt24(xfile.blockLength,False)
            f.writeInt24(xfile.blockLength2,False)
            f.writeInt24(xfile.startBlock,False)
            f.writeShort(xfile.path)
            f.writeInt(xfile.size,True,False)
            f.writeInt(xfile.updateTime,signed=False)
            f.writeInt(xfile.accessTime,signed=False)
        #overwrite any previous entries or other old data
        self.file.write('\x00'*(0x1000 - (0x40*cnt)))
        if not end:
            for b in gen:
                self.setUnusedBlocks(b)

    def checkSignature(self):
        f=self.file
        self.signatureVerified = False
        # 4 byte public exponent, 0x80 byte modulus, needs to be switched by pieces of 8
        f.seek(0x28)
        self.publicKey = pubKey = getPubKey(f.read(0x4),switchByEight(f.read(0x80)))
        # offset of signature
        f.seek(0x1AC)
        # signature is written in reverse [::-1] reverses a string
        sig = f.read(0x80)[::-1]
        hsh = sha1(f.read(0x118)).digest()
        try:
            self.signatureVerified = pubKey.verify(hsh,sig)==1
        except Exception as ex:
            return False
        return self.signatureVerified

    def fixSignature(self):
        f=self.file
        #location of data whose hash is signed
        f.seek(0x22C)
        hsh = sha1(f.read(0x118)).digest()
        f.seek(0x1AC)
        # signature written in reverse
        f.write(self.signer.sign(hsh)[::-1])
        f.seek(4)
        f.write(self.certificate)
        self.signatureVerified = True
        #I would need to resign the certificate, but I don't have the parameters for it
        #f.seek(0x28)
        #f.write(self.getSTFSPublicKeyBinary())

    def getSTFSPublicKeyBinary(self):
        e , n = [x.rjust(4, '\x00') for x in getExponentAndModulus(self.signer)]
        n = switchByEight(n)
        return e + n

    def getOffsetToVerify(self,table,level=0):
        """
        return the offset of the 0x1000 bytes that contains the block and needs to be hashed
        table: the hash table or data block number to get the verification location of
        level: level of the hash that will be hashing it
        """
        if level == 0:
            return self.getDataBlockOffset(table)
        #with the hashes, their offset will look like 0xA018, so only get the offset of the first one, 0xA000
        else: return self.getHashTableOffset(table,level-1)

    def getHashTableStart(self,block):
        return int(math.floor(float(block+1)/0xAA))

    def getHashTableOffset(self,table,level=0):
        '''
            return the offset of hashtable <table>

            self.getHashTableOffset(0,0):
                0xA000
            self.getHashTableOffset(1,0):
                0xB8000
            self.getHashTableOffset(2,0):
                0x164000

            self.getHashTableOffset(0,1):
                0xB6000
            self.getHashTableOffset(1,1):
                0x7246000
            self.getHashTableOffset(2,1):
                0xE480000

            self.getHashTableOffset(0,2):
                0x7244000
        '''
        #each hash table has 0xAA hash blocks
        return self.getHashBlockOffset(table*0xAA,level)

    def getHashBlockOffset(self,block,level = 0):
        '''
            Get the offset of a hash block.
            ex:
                block = 1, level = 0:
                    0xA018
                block = 0, level = 1:
                    0xB6000


            These are the 0x18 hash blocks, not the 0x1000 hash tables
        '''
        rtn = 0
        #cnt is the amount of blocks, while block is the zero based block number
        cnt = block + 1
        #the disposition inside the hashtable
        dsp = block % 0xAA
        if level == 2:
            rtn += 0x7244000
        elif level == 1:
            #the block is level0 hash, looking for level 1 hash

            #used1 is the amount of level1 blocks it takes to hash that amount of blocks
            #used1 = math.ceil( float(cnt) / 0xAA )


            #the amount of level1 tables that have been filled
            filled1 = math.floor(float(block) / 0xAA)
            #makes more sense to use filled, don't use used1
            #if used == 1:
                #rtn += 0xB6000

            if filled1 == 0:
                rtn += 0xB6000
            else:
                #hash tables start at 0xA000, 0x2000 for the level2 table, and 0x723A000 (0xAC000 * 0xAA + 2000), the space each level1 takes up 
                rtn += 0xA000 + 0x2000 + 0x723A000*filled1
            if self.masterHashLevel > 1:
                rtn +=0x1000 * ((self.file.seekReadByte(self.getHashBlockOffset(filled1,level+1)+0x14)>>6) &1)
        elif level == 0:
            #the block is data, we're looking for level 0 hash
            filled0 = math.floor (float(block) / 0xAA)
            #every 0x70E4 data blocks fills another level1 table
            #takes up space even when not filled though
            #filled1 = math.floor (float(cnt) / 0x70E4)


            used1 = math.ceil (float(cnt) / 0x70E4)
            if filled0 == 0:
                #we're looking for a level0 hash table and none have been filled, so it's the first
                rtn += 0xA000
            else:
                #hash tables begin at 0xA000, 0x2000 for each used level1
                rtn += 0xA000 + 0x2000 * used1 + 0xAC000 * filled0 
                if block >= 0x4AF768:
                    #level2 table :P
                    rtn+=0x2000
            if self.masterHashLevel > 0:
                if block>0x70e4:
                    print 'about to try to to find hashblock offset of {0}'.format(hex(block))
                rtn += 0x1000 * ((self.file.seekReadByte(self.getHashBlockOffset(filled0,level+1)+0x14)>>6) &1)
        rtn += 0x18 * dsp
        if self.masterHashLevel == level:
            rtn+=self.headerInfo.descriptor.masterDisposition
        #rtn += (self.headerInfo.descriptor.masterDisposition if level == self.masterHashLevel else self.hashDisposition)
        return int(rtn)

    def getDataBlockOffset(self,block):
        rtn = 0xA000
        #0xA9 is the 0xAAth block because 0x00 is the first
        if block < 0xAA:
            return 0xC000 + 0x1000 * block
        if block < 0x70E4:
            # zero indexing, so add one to find the amount of 0xAA blocks it fills
            used0 = int(math.ceil(float(block+1)/0xAA))
            '''
            0x2000 for the level 1 hash table because more than 0x70e4 blocks
            0x2000 for each hash table in the used 0xAA blocks
            0x1000 for each block
            '''
            return rtn + used0 *0x2000 + 0x2000 + 0x1000*block
        if block < 0x4AF768:
            # zero indexing, so add one to find the amount of 0xAA blocks it fills
            used0 = int(math.ceil(float(block+1)/0xAA))
            
            used1 = int(math.ceil(float(block+1)/0x70E4))
            '''
            0x2000 for the level 2 hash table because more than 0x70e4 blocks
            0x2000 for each level 0 hash table in the used 0xAA blocks
            0x2000 for each level 1 hash table in the used 0x70E4 blocks
            0x1000 for each block
            '''
            return rtn + used0 *0x2000 + 0x2000*used1 +0x2000+ 0x1000*block
        else:
            raise Exception('File too large')

    class FileEntry:
        def __init__(self,ofs=None,data=None):
            if not ofs==None:
                self.entryOffset = ofs
                self.read(data)
        def read(self,data):
            data = open2(data,True)
            #offset 0
            self.name = data.read(0x28)
            self.realName = self.name.replace('\x00','')
            #offset 0x28
            tmp = data.readByte()
            self.nameLength = tmp&0x3F
            self.flags = tmp&0xC0
            self.isDir = tmp>>7&1==1
            #offset 0x29
            #they're little endian 24 byte integers. Need to reverse them and add a zero byte to the beginning
            self.blockLength = data.readInt24(False)
            #offset 0x2C
            self.blockLength2 = data.readInt24(False)
            #offset 0x2F
            self.startBlock = data.readInt24(False)
            #offset 0x32
            self.path = data.readShort()
            #offset 0x34
            self.size = data.readInt()
            #offset 0x38
            #self.updateTime = data[0x38:0x38+4]
            self.updateTime = data.readInt(signed=False)
            #self.accessTime = data[0x3C:0x3C+4]
            self.accessTime = data.readInt(signed=False)

    class headerInfoClass:
        def __init__(self,data):
            self.read(data)
        def read(self,data):
            data = open2(data,True)
            magic = data.read(4)
            if not magic in ("CON ","LIVE","PIRS"):
                #Does this work with LIVE or PIRS even?
                raise Exception("Invalid file package, not CON, LIVE, or PIRS")
            try:
                data.seek(0xC000)
            except Exception as ex:
                #Actually, I think seeking past the file size doesn't raise an error.. Replace with another exception?
                raise Exception("File too small")
            self.headerHash = data.seekRead(0x32c, 0x14)
            self.dataFileCount = data.seekRead(0x39D,0x4)
            self.descriptor = STFSDescriptor(data.seekRead(0x379,0x24))
            

def getPubKey(e,n):
    """
    Return an M2Crypto RSA Public Key from a binary string exponent and modulus to check the signature
    """
    return M2Crypto.RSA.new_pub_key((
    M2Crypto.m2.bn_to_mpi(M2Crypto.m2.hex_to_bn(hexlify(e))),
    M2Crypto.m2.bn_to_mpi(M2Crypto.m2.hex_to_bn(hexlify(n))),
    ))

def getExponentAndModulus(pubkey):
    return (
        unhexlify(M2Crypto.m2.bn_to_hex(M2Crypto.m2.mpi_to_bn(pubkey.e))),
        unhexlify(M2Crypto.m2.bn_to_hex(M2Crypto.m2.mpi_to_bn(pubkey.n))),
        )

def switchByEight(x):
    """
    Signature and public modulus are reversed in blocks of eight bytes
    """
    if not len(x)%8==0 or len(x)==0:
        raise Exception("Error, invalid block")
    rtn=''
    for i in range(1,1+len(x)/8):
        ofs = -1*i*8
        rtn += x[ofs:None if ofs==-8 else ofs+8]
    return rtn 

class STFSDescriptor:
    def __init__(self,data):
        self.read(data)
    def read(self,data):
        data = open2(data,True)
        data.seek(0x2)
        self.blockSeparation = data.readByte()
        self.masterDisposition = 0x1000 if self.blockSeparation >> 1 == 1 else 0
        self.fileTableBlockCount = data.readShort(False)
        self.fileTableBlockNumber = data.readInt24(False)
        self.masterHash = data.read(0x14)
        self.totalAllocatedBlockCount = data.readInt()
        self.totalUnallocatedBlockCount = data.readInt()

def open2(s, mode='rb',isBinary = False):
    """
    Return a streamwrap of a binary string or file object
    """
    if isBinary == True or mode == True:
        return streamWrap (cStringIO.StringIO(s))
    else:
        return streamWrap (open(s,mode))

class streamWrap(object):
    """
    Add some reading/writing methods to cStringIO and file object
    """

    def __init__(self,stream):
        self.stream = stream

    def __getattr__(self,x):
        return getattr(self.stream,x)

    def readReverse(self,cnt):
        return self.stream.read(cnt)[::-1]

    def readInt(self,big=True,signed=True):
        typ = ('>' if big else '<') +('i' if signed else 'I')
        return struct.unpack(typ,self.read(4))[0]

    def readInt24(self,big=True,signed=True):
        typ = '>' +('i' if signed else 'I')
        #\xFFFFFF turns into \x00FFFFFF, which isn't -1
        bytes = self.read(3)[::1 if big else -1]
        return struct.unpack(typ,('\x00' if bytes[0] < '\x80' or not signed else '\xff') +bytes )[0]

    def readShort(self,big=True,signed=True):
        typ = ('>' if big else '<') +('h' if signed else 'H')
        return struct.unpack(typ,self.read(2))[0]

    def readByte(self,signed=True):
        return struct.unpack('b' if signed else 'B',self.read(1))[0]

    def seekRead(self,position,cnt):
        self.seek(position)
        return self.read(cnt)

    def seekReadInt(self,position,big=True,signed=True):
        self.seek(position)
        return self.readInt(big,signed)

    def seekReadInt24(self,position,big = True,signed=True):
        self.seek(position)
        return self.readInt24(big,signed)

    def seekReadByte(self,position,signed=True):
        self.seek(position)
        return self.readByte(signed)

    def seekReadShort(self,position,big=True,signed=True):
        self.seek(position)
        return self.readShort(big,signed)

    def seekWrite(self,position,data):
        self.seek(position)
        self.write(data)

    def writeByte(self,b,signed=True):

        self.write(struct.pack('b' if signed else 'B',b)[0])

    def writeInt(self,i,big=True,signed=True):
        typ = ('>' if big else '<') + ('i' if signed else 'I')
        self.write(struct.pack(typ,i))

    def writeInt24(self,i,big=True,signed=True):
        typ = 'i' if signed else 'I'
        self.write(struct.pack('>'+typ,i)[1:][::1 if big else -1])

    def writeShort(self,i,big=True,signed=True):
        typ = ('>' if big else '<') + ('h' if signed else 'H')
        self.write(struct.pack(typ,i))

    def getInt24(self,i,big=True,signed=True):
        typ = '>' +('i' if signed else 'I')
        #\xFFFFFF turns into \x00FFFFFF, which isn't -1
        bytes = i[::1 if big else -1]
        return struct.unpack(typ,('\x00' if bytes[0] < '\x80' or not signed else '\xff') +bytes )[0]

    def makeInt24(self,i,big=True,signed=True):
        typ = 'i' if signed else 'I'
        return struct.pack('>'+typ,i)[1:][::1 if big else -1]

'''
#no point, use cStringIO
class stringStream:
    def __init__(self,s):
        self.__str = s
        self.__position=0
        self.size = len(s)
    def seek(self,pos):
        self.__position=pos
    def read(self,cnt=None):
        if cnt == None:
            rtn=self.__str[self.__position:]
            self.__position = len(self.__str)
            return rtn
        rtn = self.__str[self.__position:self.__position+cnt]
        self.__position+=cnt
        return rtn
    def readReverse(self,cnt):
        #if cnt==len(self.__str): return self.__str
        #return self.__str[self.__position:self.__position+cnt][::-1]
        #return self.read(4)[::-1]
        return self.read(cnt)[::-1]
    def readInt(self,big=True,signed=True):
        typ = 'i' if signed else 'I'
        return struct.unpack(('>' if big else '<') +typ,self.read(4))[0]
    def readInt24(self,big=True,signed=True):
        typ = 'i' if signed else 'I'
        return struct.unpack('>'+typ,'\x00'+(self.read(3)[::1 if big else -1]))[0]
    def readShort(self,big=True,signed=True):
        typ = 'h' if signed else 'H'
        return struct.unpack('>'+typ,self.read(2)[::1 if big else -1])[0]
    def readByte(self,signed=True):
        return struct.unpack('b' if signed else 'B',self.read(1))[0]
    def seekRead(self,position,cnt):
        self.__position = position
        return self.read(cnt)
    def tell(self):
        return self.__position
'''
def logTime(msg):
    global startTime
    print msg + '\t'+str(time.time()-startTime)
def getHash(pos,len=0x1000):
    global x
    f=x.file
    #f=open(pf,'rb')
    f.seek(pos)
    print 'hash: '+hexlify(sha1(f.read(len)).digest())


startTime = time.time()
x=None

def a(b): return '{0} : {1}'.format(hex(x.getDataBlockOffset(b)),hex(x.getHashBlockOffset(b)))
def main():
    global x
    x=QuickSTFS(files.pf2)
    #x.addFile(wp)
    #x.writeFileTable()
    #x.extractFirst('/test/buggars/')
#cProfile.run('main()')
#main()

def tst():
    global x
    import files
    x=QuickSTFS(files.m)
    x.addFile(files.mins,x.files[0])

